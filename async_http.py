import aiohttp
import asyncio


async def get_empty():
    async with aiohttp.ClientSession() as session:
        print("#1 get without headers")
        async with session.get('http://127.0.0.1:8000') as resp:
            print(await resp.text())


async def get_with_header():
    async with aiohttp.ClientSession() as session:
        print("#2 get with headers")
        async with session.get('http://127.0.0.1:8000',
                               json={"user": "Jime"}) as resp:
            print(await resp.text())


async def post_bad1():
    async with aiohttp.ClientSession() as session:
        print("#3 POST bad one")

        async with session.post("http://127.0.0.1:8000",
                                json={"user": "Jime"}) as resp:
            print(await resp.text())


async def post_bad2():
    async with aiohttp.ClientSession() as session:
        print("#3_1 POST bad one")

        async with session.post("http://127.0.0.1:8000",
                                json={}) as resp:
            print(await resp.text())


async def post_good():
    async with aiohttp.ClientSession() as session:
        print("#3 POST good ")
        from random import choice
        params = choice([{"hello": "world"},
                         {"Where are you go?": "home"},
                         {"The foxes": "are broun "}])

        async with session.post("http://127.0.0.1:8000",
                                json={"user": "Jime",
                                      "data": params},
                                ) as resp:

            print(await resp.text())


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    # loop.run_until_complete(info_request())

    print("="*40)
    loop.run_until_complete(get_empty())
    print("="*40)
    loop.run_until_complete(get_with_header())
    print("="*40)
    loop.run_until_complete(post_bad1())
    print("="*40)
    loop.run_until_complete(post_bad2())
    print("="*40)
    loop.run_until_complete(post_good())
