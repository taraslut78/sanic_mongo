from sanic.views import HTTPMethodView
from sanic.response import json
from sanic_openapi import doc
# from app import app


class MyApp(HTTPMethodView):

    # @app.get("/")
    @doc.summary("Get users from model")
    @doc.description("Get users from model")
    @doc.consumes({"name": str}, location='body')
    async def get(self, request):
        data = request.json
        if data is None:
            return json({"data": "user is undefined"},
                        status=400)

        user_name = data.get("user")
        if user_name is None:
            return json({"data": "user is undefined"},
                        status=400)

        user_name_collections = await request.app.db.list_collection_names()
        if user_name in user_name_collections:
            docs = await request.app.db[user_name].find({}).to_list(1000)
            return json({"data": str(docs)},
                        status=200)

        return json({"data": "user is undefined"},
                    status=400)

    @doc.description("Create new user & his job.")
    async def post(self, request):
        data = request.json
        if data is None:
            return json({"data": "there is no input data"}, status=400)

        user_name = data.get("user")
        if user_name is None:
            return json({"data": "user is undefined"}, status=400)

        data_for_insert = data.get("data")
        user_name_collections = await request.app.db.list_collection_names()
        if user_name not in user_name_collections:
            await request.app.db[user_name].insert_one({"test": "test"})
            await request.app.db[user_name].delete_many({"test": "test"})
            return json({"data": "user is just created"}, status=200)

        if data_for_insert:
            inserted_id = await request.app.db[user_name].insert_one(data_for_insert)
            if inserted_id:
                return json({"data": "done",
                             "id": str(inserted_id.inserted_id)},
                            status=200)
            return json({"data": "data do NOT saved"}, status=400)
        return json({"data": "There are some errors"}, status=400)

    @doc.description("Delete job by ID")
    async def delete(self, request):
        data = request.json
        user_name = data.get("name")
        if user_name is None:
            return json({"data": "user is undefined"}, status=400)

        user_name_collections = await request.app.db.list_collection_names()
        if user_name in user_name_collections:
            num = await request.app.db[user_name].drop()
            if num:
                return json({"data": "user is deleted"}, status=200)
            return json({"data": "There are some errors"}, status=400)
        return json({"data": "user is not registrated"}, status=400)
