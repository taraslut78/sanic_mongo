from pprint import pprint
from sanic import Sanic, Blueprint
from motor.motor_asyncio import AsyncIOMotorClient
from service import MyApp
from sanic_openapi import swagger_blueprint, openapi_blueprint

app = Sanic(__name__)

MONGO_URI = "127.0.0.1"
MONGO_PORT = 27017


@app.listener("after_server_start")
async def init(app: Sanic, loop):
    client = AsyncIOMotorClient(MONGO_URI, MONGO_PORT, io_loop=app.loop)
    app.db = client.users


@app.listener("before_server_stop")
async def close_connection(app, loop):
    pass
    # app.db.close()

api_v1 = Blueprint("v1")
api_v1.add_route(MyApp.as_view(), "/")

app.blueprint(api_v1)
app.blueprint(openapi_blueprint)
app.blueprint(swagger_blueprint)


if __name__ == "__main__":
    app.run(host="127.0.0.1", port=8000, debug=True)
