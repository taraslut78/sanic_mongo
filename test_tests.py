import pytest
import asyncio
from app import app


# @pytest.yield_fixture(scope="function")
# def sanic_app():
#     yield app
#
#
# @pytest.fixture(scope="function")
# def test_cli(sanic_app, test_client):
#     loop = asyncio.get_event_loop()
#     return loop.run_until_complete(test_client(sanic_app))


async def test_index_get_401():
    loop = asyncio.get_event_loop()
    print("#1")
    data = {"name": ""}
    # response = loop.run_until_complete(test_cli.get("/", json=data))
    request, response = app.test_client.get("/", json=data)
    assert response.status == 400
    # assert True

# async def test_index_empty_post():
#     print("#2 ")
#     request, response = app.test_client.post("/")
#     assert response.status == 400
#
#
# async def test_index_empty_post2():
#     print("#2-1")
#     request, response = app.test_client.post("/",
#                                              json={"task": 12,
#                                                    "description": "hello"})
#     assert response.status == 400
#
#
# async def test_positive_post():
#     print("#3 ")
#     request, response = app.test_client.post("/",
#                                              json={"name": "Ivan",
#                                                    "data": {"task": 12, "description": "hello"}},
#                                              )
#     assert response.status == 201
#
#
# async def test_index_get_200():
#     print("#4")
#     head = {"name": "Ivan"}
#     request, response = app.test_client.get("/", headers=head)
#     assert response.status == 200, "awaited 200, but got response " + str(response.status)
#
#
# async def test_index_not_delete():
#     print("#5")
#     request, response = app.test_client.delete("/")
#     assert response.status == 204
#
#
# async def test_index_delete():
#     print("#6")
#     head = {"name": "Ivan"}
#     request, response = app.test_client.delete("/",
#                                                headers=head)
#     assert response.status == 200
#
#
# async def test_index_bad_delete():
#     print("#7")
#     head = {"name": "dfgdfg"}
#     request, response = app.test_client.delete("/",
#                                                headers=head)
#     assert response.status == 204


# if __name__ == "__main__":
#     # Uncomment next line if the previous test had failed
#     # test_index_delete()
#
#     test_index_get_401()
#     test_index_empty_post()
#     test_index_empty_post2()
#     test_positive_post()
#     test_index_get_200()
#     test_index_not_delete()
#     test_index_delete()
#     test_index_get_401()
#     test_index_bad_delete()
#
